import concurrent.futures as cf

import wikipedia

from .decorators import run_in_thread
from .worker import executor
from .utils import unique

def refers_to_an_article(title):
    """Check whether the page refers to an article
    (as opposed to a special page / disambiguation)
    """

    return not (
        title.startswith('All pages with titles ')
        or title.endswith(' (disambiguation)')
    )

def construct_article_url(title):
    """Create article URL from title."""

    encoded_title = title.replace(' ', '_')
    return f'https://en.wikipedia.org/wiki/{encoded_title}'

@run_in_thread(executor)
def get_page_details(title):
    """Get page details.

    Given an article title, return a dictionary
    with the title, URL, pageid, summary, and an image.

    If the page cannot load or must be disambiguated,
    it is skipped and None is returned.

    Because of the @run_in_thread decorator,
    calling this function will immediately return a promise
    while the function runs in the background.
    The actual return value from the function can be
    obtained by calling promise.result().
    """
    try:
        page = wikipedia.page(title)

        try:
            images = page.images
            # The list of images doesn't seem to be ordered,
            # so this is just one random image from the article
            image = images[0] if len(images) > 0 else None
        except KeyError:
            # Some pages without images cause a KeyError to be thrown
            # when attempting to access page.images
            image = None

        details = {
            'title': page.title,
            'url': page.url,
            'pageid': page.pageid,
            'image': image,
            'summary': page.summary
        }
        return details

    except wikipedia.PageError:
        # The page could not be loaded (broken link?)
        return
    except wikipedia.DisambiguationError:
        # This is actually a disambiguation page
        # In this case, we'll just assume that
        # relevant articles are already showing up
        # as other titles in the search results,
        # rather than recursively disambiguating.
        return
def perform_search(term, detail=False):
    """
    Perform a Wikipedia search for the given term.

    If detail=False, just a list of article URLs
    will be returned.

    If detail=True, each result will be a dictionary
    with the fields described in `get_page_details`.
    The HTTP requests to load these pages are
    processed in parallel via multi-threading.
    """

    all_search_result_titles = wikipedia.search(term)
    search_result_article_titles = filter(
        refers_to_an_article,
        all_search_result_titles
    )
    if detail:
        # Detail view involves HTTP requests
        # which can be run simultaneously
        # using our @run_in_thread decorator.

        # Call all page fetch functions,
        # which start running in threads,
        # and immediately return promises without blocking
        page_detail_promises = [
            get_page_details(title)
            for title in search_result_article_titles
        ]
        # After all functions have been started,
        # wait for them to all finish before returning.
        page_details = []
        for promise in page_detail_promises:
            result = promise.result()
            if result is not None:
                page_details.append(result)

        return page_details

    else:
        # If detail view is not required,
        # URL can be easily constructed without
        # HTTP requests, so we can proceed synchonosly.
        urls = [
            construct_article_url(title)
            for title in search_result_article_titles
        ]
        return urls
