import http
import math

from flask import Flask
from parse import parse

from .decorators import http_decorator
from .search import perform_search
from .worker import executor

app = Flask(__name__)
HOSTNAME = 'wikisearch.us'

# Dummy response for health check (readiness probe)
@app.route('/healthz', methods=['GET'])
def healthz():
    return ('', http.HTTPStatus.NO_CONTENT)

class HostMismatchError(ValueError):
    pass

@app.route('/', methods=['GET'])
@http_decorator(user_errors=[HostMismatchError], args_from_headers=['Host'])
def wiki_search(Host: str, detail: bool = False):
    host_pattern = '{search_term}.{hostname}'
    parse_result = parse(host_pattern, Host)
    search_term = parse_result['search_term']
    if parse_result is None or parse_result['hostname'] != HOSTNAME:
        raise HostMismatchError("Host does not match expected pattern")
    results = perform_search(search_term, detail)
    return results
