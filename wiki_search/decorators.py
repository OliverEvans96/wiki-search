import functools as ft
import inspect
import json

from flask import request, Response
import uuid
from werkzeug.exceptions import BadRequest

class FieldValidationError(TypeError):
    """A required field is missing or has the wrong type."""
    pass

def run_in_thread(executor):
    """Add the function, args, and kwargs
    to the queue to be processed by a worker thread.
    """
    def inner_decorator(func):
        @ft.wraps(func)
        def wrapper(*args, **kwargs):
            future = executor.submit(func, *args, **kwargs)
            return future
        return wrapper
    return inner_decorator

def http_decorator(user_errors=(), args_from_headers=()):
    """Handle HTTP status codes, logging, and input handling.

    Parameters
    ----------
    user_errors: iterable of Error types
        If any of these errors are raised, an HTTP 400 Bad Request will be returned.
        If any other error is raised, an HTTP 500 Internal Server Error will be returned.
        Additionally, a 400 will be returned if invalid arguments are  supplied.

    args_from_headers: iterable of strings
        The given strings should be the names of parameters of the decorated function.
        These parameters will be retreived from HTTP Headers rather than GET arguments.
    """
    all_user_errors = (*user_errors, FieldValidationError)
    def inner_decorator(func):
        @ft.wraps(func)
        def wrapper():
            # Generate random UUID in order
            # to link request & response
            request_id = uuid.uuid4()
            try:
                headers = dict(request.headers)
                parameters = dict(request.args)
                fields = inspect_fields(func)
                header_fields = {
                    header_name: fields.pop(header_name)
                    for header_name in args_from_headers
                }
                # Strip extra headers that weren't requested as arguments
                headers = {
                    key: value
                    for key, value in headers.items()
                    if key in header_fields
                }
                validate_fields(parameters, fields, source='GET parameters')
                validate_fields(headers, header_fields, source='request headers')
                data = {**headers, **parameters}
                print("Request  ({}): {}".format(request_id, data))
                result = func(**data)
                print("Response ({}): {}".format(request_id, result))
                return success(result)
            except all_user_errors as e:
                msg = str(e)
                print("400 {} ({}): {}".format(type(e).__name__, request_id, msg))
                return user_error(msg)
            except BadRequest as e:
                # BadRequest needs to be handled separately
                # for consistent formatting
                msg = str(e.description)
                print("400 {} ({}): {}".format(type(e).__name__, request_id, msg))
                return user_error(msg)
            #except Exception as e:
            #    msg = str(e)
            #    print("500 {} ({}): {}".format(type(e).__name__, request_id, msg))
            #    return server_error(msg)
        return wrapper
    return inner_decorator

def inspect_fields(func):
    """Get dict of argument names, types, and whether they are required from annotated function signature"""
    params = inspect.signature(func).parameters
    fields = {
        name: {
            "type": param.annotation,
            "required": param.default is inspect._empty
        }
        for name, param in params.items()
    }
    return fields

def validate_fields(data, fields, source):
    """Check that fields of correct types are present in dict"""
    if data is None or not isinstance(data, dict):
        return user_error("{} missing.".format(source))
    else:
        for field, field_info in fields.items():
            field_type = field_info["type"]
            field_required = field_info["required"]
            # Check that field is present if required
            field_present = field in data
            if field_required and not field_present:
                raise FieldValidationError("Missing field '{}' from {}".format(field, source))
            # Check that field has the correct type
            # (only if an annotation is present)
            if field_present and field_type != inspect._empty:
                if not isinstance(data[field], field_type):
                    # Attempt conversion before raising an exception
                    try:
                        data[field] = field_type(data[field])
                    except TypeError as e:
                        if isinstance(field_type, type):
                            type_string = field_type.__name__
                        elif isinstance(field_type, (tuple, list)):
                            type_string = "' or '".join([t.__name__ for t in field_type])
                        else:
                            # In this case, type annotation is invalid, so don't raise an error for the user
                            print(f"ERROR: Invalid type annotation '{field_type}' ({type(field_type)}). Not checking.")
                            continue
                        raise FieldValidationError("Field '{}' in {} must be of type '{}'".format(field, source, type_string))

def success(payload):
    return Response(
        response=json.dumps(payload),
        status=200,
        mimetype='application/json'
    )

def user_error(msg):
    return Response(
        response=json.dumps({
            'status': 400,
            'error': msg
        }),
        status=400,
        mimetype='application/json'
    )

def server_error(msg):
    return Response(
        response=json.dumps({
            'status': 500,
            'error': msg
        }),
        status=500,
        mimetype='application/json'
    )
