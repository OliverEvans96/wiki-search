def unique(lst):
    """Return a list of unique elements from the input list."""
    return list(set(lst))

