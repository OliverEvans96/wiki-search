# Wikipedia Subdomain Searcher

Flask app that searches wikipedia based on the subdomain at which it's accessed.

See it live: http://potato.wikisearch.us

*NOTE*: Check out the `?detail=y` GET parameter for more info. e.g.: http://tesseract.wikisearch.us?detail=y

## Repository Overview

This repo contains code to run a python web server, 
as well as to automatically build and deploy the server reproducibly 
with Docker + Kubernetes whenever a new commit is pushed ([example pipeline](https://gitlab.com/OliverEvans96/wiki-search/pipelines/97489662/builds)).

Here are some highlights from the repo!

### Python Code
- Small, concise functions
- Proper documentation
- Fancy, useful decorators
- Futures-based multi-threading
- Request + Response logging

### Python Package
- setup.py, requirements.txt
- Install via `pip install .`

### Dockerfile
- Install dependencies & this package
- Tag by git SHA for reproducibility

### Kubernetes YAML
- Pull docker image by SHA, run gunicorn
- Several replicas round-robin load balanced

### Gitlab CI
Automatically do the following on `git push`:
- Build docker image
- Push image to container registry
- Deploy Kubernetes resources to new version (rolling update)

## Examples

This is how it looks. But don't take my word for it, [try it out!](http://octopus.wikisearch.us?detail=y)

### Server logs

```
[2019-11-21 00:53:07 -0800] [190272] [INFO] Starting gunicorn 19.9.0
[2019-11-21 00:53:07 -0800] [190272] [INFO] Listening at: http://0.0.0.0:8008 (190272)
[2019-11-21 00:53:07 -0800] [190272] [INFO] Using worker: sync
[2019-11-21 00:53:07 -0800] [190276] [INFO] Booting worker with pid: 190276
Request  (32492277-cfbf-4b14-9509-2cd28edc904e): {'Host': 'pickle.wikisearch.us'}
Response (32492277-cfbf-4b14-9509-2cd28edc904e): ['https://en.wikipedia.org/wiki/Pickle', 'https://en.wikipedia.org/wiki/Pickled_cucumber', 'https://en.wikipedia.org/wiki/Pickling', 'https://en.wikipedia.org/wiki/Pickle_Rick', 'https://en.wikipedia.org/wiki/Pickles', 'https://en.wikipedia.org/wiki/The_Pickle', 'https://en.wikipedia.org/wiki/Branston_(brand)', 'https://en.wikipedia.org/wiki/Christmas_pickle', 'https://en.wikipedia.org/wiki/Pickleball', 'https://en.wikipedia.org/wiki/South_Asian_pickles']
Request  (636b98f4-0e24-49ee-9687-362bdd13e099): {'Host': 'carpet.wikisearch.us'}
Response (636b98f4-0e24-49ee-9687-362bdd13e099): ['https://en.wikipedia.org/wiki/Carpet', 'https://en.wikipedia.org/wiki/The_Carpet', 'https://en.wikipedia.org/wiki/Carpet_beetle', 'https://en.wikipedia.org/wiki/Red_carpet', 'https://en.wikipedia.org/wiki/Varied_carpet_beetle', 'https://en.wikipedia.org/wiki/Persian_carpet', 'https://en.wikipedia.org/wiki/Magic_carpet', 'https://en.wikipedia.org/wiki/Tennis_court', 'https://en.wikipedia.org/wiki/Berber_carpet']
```

### Client logs

```
% curl -s 'closed_timelike_curve.wikisearch.us' | jq
[
  "https://en.wikipedia.org/wiki/Closed_timelike_curve",
  "https://en.wikipedia.org/wiki/Chronology_protection_conjecture",
  "https://en.wikipedia.org/wiki/Anti-de_Sitter_space",
  "https://en.wikipedia.org/wiki/Gödel_metric",
  "https://en.wikipedia.org/wiki/Novikov_self-consistency_principle",
  "https://en.wikipedia.org/wiki/Wormhole",
  "https://en.wikipedia.org/wiki/Causal_loop",
  "https://en.wikipedia.org/wiki/Causal_structure",
  "https://en.wikipedia.org/wiki/Timelike_homotopy",
  "https://en.wikipedia.org/wiki/Homotopy"
]

% curl -s 'laughter.wikisearch.us?detail=y' | jq
[
  {
    "title": "Laughter",
    "url": "https://en.wikipedia.org/wiki/Laughter",
    "pageid": "234801",
    "image": "https://upload.wikimedia.org/wikipedia/commons/1/12/Bundesarchiv_Bild_183-T0425-0005%2C_Grevesm%C3%BChlen%2C_Bekleidungswerk%2C_Wettbewerb.jpg",
    "summary": "Laughter is a physical reaction in humans consisting typically of rhythmical, often audible contractions of the diaphragm and other parts of the respiratory system. It is a response to certain external or internal stimuli. Laughter can arise from such activities as being tickled, or from humorous stories or thoughts. Most commonly, it is considered a visual expression of a number of positive emotional states, such as joy, mirth, happiness, relief, etc. On some occasions, however, it may be caused by contrary emotional states such as embarrassment, apology, or confusion such as nervous laughter or courtesy laugh. Age, gender, education, language, and culture are all indicators as to whether a person will experience laughter in a given situation. Some other species of primate (chimpanzees, gorillas, and orangutans) show laughter-like vocalizations in response to physical contact such as wrestling, play chasing or tickling.\nLaughter is a part of human behavior regulated by the brain, helping humans clarify their intentions in social interaction and providing an emotional context to conversations. Laughter is used as a signal for being part of a group—it signals acceptance and positive interactions with others. Laughter is sometimes seen as contagious, and the laughter of one person can itself provoke laughter from others as a positive feedback.The study of humor and laughter, and its psychological and physiological effects on the human body, is called gelotology."
  },
  {
    "title": "Death from laughter",
    "url": "https://en.wikipedia.org/wiki/Death_from_laughter",
    "pageid": "647417",
    "image": "https://upload.wikimedia.org/wikipedia/commons/c/c3/Chrysippus_of_Soli.jpg",
    "summary": "Death from laughter is a rare form of death, usually resulting from cardiac arrest or asphyxiation, caused by a fit of laughter. Instances of death by laughter have been recorded from the times of ancient Greece to the modern day.\n\n"
  },
  {
    "title": "Nervous laughter",
    "url": "https://en.wikipedia.org/wiki/Nervous_laughter",
    "pageid": "4229016",
    "image": null,
    "summary": "Nervous laughter is laughter evoked from an audience's expression of alarm, embarrassment, discomfort or confusion, rather than amusement.  Nervous laughter is usually less robust in expression than \"a good belly laugh\", and may be combined with confused glances or awkward silence on the part of others in the audience.  Nervous laughter is considered analogous to a courtesy laugh, which may be rendered by more of a conscious effort in an attempt to move a situation along more quickly, especially when the comedian is pausing for laughter.\nNervous laughter is a physical reaction to stress, tension, confusion, or anxiety. Neuroscientist Vilayanur S. Ramachandran states \"We have nervous laughter because we want to make ourselves think what horrible thing we encountered isn't really as horrible as it appears, something we want to believe.\" Those are the most embarrassing times, too, naturally.\nPsychologist and neuroscientist Robert Provine, from the University of Maryland, studied over 1,200 \"laughter episodes\" and determined that 80% of laughter isn't a response to an intentional joke.Unhealthy or \"nervous\" laughter comes from the throat. This nervous laughter is not true laughter, but an expression of tension and anxiety. Instead of relaxing a person, nervous laughter tightens them up even further. Much of this nervous laughter is produced in times of high emotional stress, especially during times where an individual is afraid they might harm another person in various ways, such as a person's feelings or even physically.People laugh when they need to project dignity and control during times of stress and anxiety. In these situations, people usually laugh in a subconscious attempt to reduce stress and calm down, however, it often works otherwise. Nervous laughter is often considered fake laughter and even heightens the awkwardness of the situation.People may laugh nervously when exposed to stress due to witnessing others' pain. For instance, in Stanley Milgram's obedience experiment, subjects (\"teachers\") were told to shock \"learners\" every time the learners answered a question incorrectly.  Although the \"learners\" were not actually shocked, the subjects believed they were. As they were going through the study, many of the \"subjects showed signs of extreme tension and conflict\". Milgram observed some subjects laughing nervously when they heard the \"learners'\" false screams of pain. In A Brief Tour of Human Consciousness, neuroscientist V.S. Ramachandran suggests that laughter is used as a defense mechanism used to guard against overwhelming anxiety. Laughter often diminishes the suffering associated with a traumatic event.If the individual is shy or bashful and appears nervous when talking, they are likely to exhibit nervous laughter. Individuals who are shy and introverted who find themselves the center of conversational attention often become \"giddy\" with nervous laughter; this is a subconscious response caused by the brain over-thinking due to social anxiety or inexperience."
  },
  {
    "title": "Laugh track",
    "url": "https://en.wikipedia.org/wiki/Laugh_track",
    "pageid": "263909",
    "image": "https://upload.wikimedia.org/wikipedia/commons/5/55/72843_lonemonk_approx-800-laughter-only-1.wav",
    "summary": "A laugh track (or laughter track) is a separate soundtrack for a recorded comedy show containing the sound of audience laughter. In some productions, the laughter is a live audience response instead; in the United States, where it is most commonly used, the term usually implies artificial laughter (canned laughter or fake laughter) made to be inserted into the show. This was invented by American sound engineer Charles \"Charley\" Douglass.\nThe Douglass laugh track became a standard in mainstream television in the U.S., dominating most prime-time sitcoms from the late 1950s to the late 1970s. Usage of the Douglass laughter decreased by the 1980s when stereophonic laughter was provided by rival sound companies as well as the overall practice of single-camera sitcoms eliminating audiences altogether."
  },
  ...
]
```

## Thank you!

Thanks for reading, and I hope you have a great day! :)

Oliver
