FROM continuumio/miniconda3
ARG GIT_COMMIT
ENV GIT_COMMIT=$GIT_COMMIT
ENV PYTHONUNBUFFERED=TRUE
RUN mkdir /tmp/mypkg
COPY . /tmp/mypkg
RUN pip install -r /tmp/mypkg/requirements.txt && pip install -e /tmp/mypkg
CMD ["/bin/sh"]
