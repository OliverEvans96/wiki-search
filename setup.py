from distutils.core import setup

setup(
    name='wiki-search',
    version='0.1',
    license='None (All rights reserved)',
    description="Flask app that searches wikipedia based on the subdomain at which it's accessed",
    packages={'wiki_search'},
    install_requires=[
        'flask',
        'gunicorn',
        'uuid',
        'parse',
        'wikipedia',
    ]
)
